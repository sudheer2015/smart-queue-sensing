import sys
sys.path.append("./src")

import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt

import numpy as np
from numpy import exp, array, random, dot
import tkinter as tk
import scipy.misc
import os
import real_time_face_recognition as fr


class DisplayActivationFunctions:
    def __init__(self, root, master, *args, **kwargs):
        self.master = master
        self.root = root
        #########################################################################
        #  Set up the constants and default values
        #########################################################################
        self.xmin = 0
        self.xmax = 150
        self.ymin = 0
        self.ymax = 2
        self.alpha_rate = 0.1
        self.delay_elements = 10#0-100
        self.training_sample=80 #0-100%
        self.batch_size=100 # 0-200
        self.iterations=10
        self.bias = 0
        self.trgData =0  # trgData holder
        self.testData =0 # testData holder
        self.nodes=2
        self.in_size=self.delay_elements+1
        self.trgDataLen=0
        self.testDataLen =0
        
        #########################################################################
        #  Set up the plotting area
        #########################################################################
        self.plot_frame = tk.Frame(self.master)
        self.plot_frame.grid(row=0, column=0, columnspan=3, sticky=tk.N + tk.E + tk.S + tk.W)
        self.plot_frame.rowconfigure(0, weight=1)
        self.plot_frame.columnconfigure(0, weight=1)
        self.figure = plt.figure("")
        self.axes = self.figure.gca()
        self.axes.set_xlabel('Input')
        self.axes.set_ylabel('Output')
        self.axes.set_title("")
        plt.xlim(self.xmin, self.xmax)
        plt.ylim(self.ymin, self.ymax)
        self.canvas = FigureCanvasTkAgg(self.figure, master=self.plot_frame)

        self.plot_widget = self.canvas.get_tk_widget()
        self.plot_widget.grid(row=0, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
        #########################################################################
        #  Set up the frame for sliders (scales)
        #########################################################################
        self.sliders_frame = tk.Frame(self.master)
        self.sliders_frame.grid(row=1, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
        self.sliders_frame.rowconfigure(0, weight=10)
        self.sliders_frame.rowconfigure(1, weight=5)
        self.sliders_frame.columnconfigure(0, weight=5, uniform='xx')
        self.sliders_frame.columnconfigure(1, weight=5, uniform='xx')
        self.errors = []
        

        self.buttons_frame = tk.Frame(self.master)
        self.buttons_frame.grid(row=1, column=1, sticky=tk.N + tk.E + tk.S + tk.W)
        self.buttons_frame.rowconfigure(1, weight=5)
        self.buttons_frame.columnconfigure(1, weight=5, uniform='xx')
        self.buttons_frame.columnconfigure(0, weight=5, uniform='xx')

#        #-----------------My buttons - train ------------------------------------------------
        self.train_variable = tk.DoubleVar()
        self.trainButton=tk.Button(self.buttons_frame, text="Test from Camera", command=self.online_callback)
        self.trainButton.grid(row=0, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
        print("Window size:", self.master.winfo_width(), self.master.winfo_height())
        #------------Button Random --------------------------------------------
        self.zero_weights = tk.DoubleVar()
        self.zero_weights_Button = tk.Button(self.buttons_frame, text="Display analytics", command=self.offline_callback)
        self.zero_weights_Button.grid(row=0, column=1, sticky=tk.N + tk.E + tk.S + tk.W)
        print("Window size:", self.master.winfo_width(), self.master.winfo_height())
        #-------------------------------end my Buttons---------------------------------------
        
    def online_callback(self):
        #fr.test_online_image()
        fr.detect_faces_online_image()

        self.root.status_bar.set("%s", "runing from camera ")

    def offline_callback(self):
        self.plot_analytics()

    def plot_analytics(self):
        plt.clf()
        self.axes.cla()

        data = []
        for temp in open("src/data/log.txt"):
            data.append(temp.strip('\n'))

        print(data)
        # https://stackoverflow.com/questions/17713873/how-do-i-sort-a-python-list-of-time-values
        # sorted_data = sorted((time.strftime(d.split(":")[0] + ":" + d.split(":")[1], "%H:%M") for d in data))

        x_values = [temp.split(":")[0] + ":" + temp.split(":")[1] for temp in data]
        y_values = [temp.split(":")[2] for temp in data]

        x_values_count = {}
        for temp in range(len(x_values)):
            if x_values[temp] not in x_values_count.keys():
                x_values_count[x_values[temp]] = int(y_values[temp])
            else:
                x_values_count[x_values[temp]] += int(y_values[temp])

        x_values_count = dict(sorted(x_values_count.items()))

        x_values = x_values_count.keys()
        y_values = x_values_count.values()

        print(x_values, y_values)
        print(len(x_values), len(y_values))

        plt.plot([], [])
        plt.scatter(x_values, y_values)

        self.axes.xaxis_date()
        plt.gcf().autofmt_xdate()

        plt.tight_layout()
        self.canvas.draw()
