# coding=utf-8
"""Performs face detection in realtime.

Based on code from https://github.com/shanren7/real_time_face_recognition
"""
# MIT License
#
# Copyright (c) 2017 François Gervais
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import argparse
import sys
import time

import cv2
import time
import myface
import face
import math
import pyrebase

def add_overlays(frame, faces):
    name=[]
    if faces is not None:
        for face in faces:
            face_bb = face.bounding_box.astype(int)

            cv2.rectangle(frame,
                          (face_bb[0], face_bb[1]), (face_bb[2], face_bb[3]),
                          (0, 255, 0), 2)
            if face.name is not None:
                cv2.putText(frame, face.name, (face_bb[0], face_bb[3]),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0),
                            thickness=2, lineType=2)
                name =face.name

    return name

def detect_faces_online_image():
    import numpy as np
    camera_port = 0
    ramp_frames = 3
    frame_to_capture = 10
    print("going now now nowan")
    camera = cv2.VideoCapture(0)
    face_dection = myface.myDetection() # model object created
    for i in range(ramp_frames):
        retval, im = camera.read()
    
    # single frame capture of faces
    while True:
        total_faces = 0   
        for j in range(frame_to_capture):
            retval, im = camera.read()
            time.sleep(0.005)
            faces = face_dection.count_faces(im)
            total_faces += len(faces)

        total_faces = math.ceil(total_faces/frame_to_capture)

        # Read system time and write to log start
        from datetime import datetime
        current_date = str(datetime.now().time()).split(":")
        processed_time = current_date[0:2]
        processed_time = processed_time[0] + ":" + processed_time[1]

        file = open("src/data/log.txt", 'a')
        file.write(processed_time + ":" + str(total_faces) + '\n')
        # file.close()
        # Read system time and write to log end

        connect(str(total_faces))
        print ("There are ", total_faces , "persons in Queue waiting for checkout" )
        add_overlays(im, faces)
        cv2.imshow("Video", im)

        # Using wait key to delay 10 seconds instead of sleep
        k = cv2.waitKey(10000)
        cv2.destroyAllWindows()

        if k == 27:
            cv2.destroyAllWindows()
            break
        # time.sleep(10)

def connect(no_faces):
    config = {
                "apiKey": "apiKey",
                "authDomain": "facerecogpoc.firebaseapp.com",
                "databaseURL": "https://facerecogpoc.firebaseio.com/",
                "storageBucket": "facerecogpoc.appspot.com"
              }

    firebase = pyrebase.initialize_app(config)

    # Create db object
    db = firebase.database()

    # Before update data
    print("Before: ", db.child("faceCount").get().val())

    # update data
    db.update({"faceCount": no_faces})

    # After update data
    print("After: ", db.child("faceCount").get().val())
