# Smart Queue Sensing

This application uses **'Multi CNN'** pretrained model to detect faces in real-time images. The information is updated to firebase database in real-time. Customers can check queue status using our android app from their android devices.

## Motivation

Many industries look forward to provide their customers with ever higher levels of convenience. Take for instance the front desk at a hotel. Wouldn’t be nice if the customers, before they even stepped out of their beds, could know beforehand how many people were in line at the front desk, waiting for checking-in / checking-out? This obviously would represent not only a very convenient information source for a better educated decision by the guests considering showing up at the front desk, who could now check such information on their mobile devices. Further expand this, and you could apply the same convenience to the hotel’s gym, restaurant, bar, restrooms, laundry and so forth. Also, new situational awareness for the hotel staff itself could be made available, as it would now be able to follow these trends in real time and over time, thus being able to make all sorts of improvements to reduce the guests’ waiting time. Expand this same technique and rationale to other industries, and the potential is immediately clear.

## Requirements

> **Note:** These requirements are for Ubuntu Budgie 19.04. If you are using a different OS, then requirements can vary.

> **Error:** OpenCV(3.4.1) Error: Unspecified error (The function is not implemented. Rebuild the library with Windows, GTK+ 2.x or Carbon support. If you are on Ubuntu or Debian, install libgtk2.0-dev and pkg-config, then re-run cmake or configure script) in cvShowImage, file /opt/conda/conda-bld/opencv-suite_1527005194613/work/modules/highgui/src/window.cpp, line 636

> **Note:** If you receive the above error on line **'cv2.imshow("Video", im)'**, it means you are on OpenCV 3.4.1 and your system GTK is in higher version. You have two problems to solve this error, install openCV from source or just install openCV 4.0.0. I recommend choosing the later.

* Python 3.6.5
* OpenCV 4.0.0
* tensorflow 1.9.0
* pyrebase
* facerecogpoc app on android phone

Download the **'com.example.ramjiseetharaman.facerecogpoc.apk'** from **'apk'** folder and install it on your phone. 

This application will connect to our firebase database on launch and will start streaming queue status.

> **Note:** This application uses internet to communicate information from server to your phone and does not require granting any other permissions.  

## Application Demo

<p align="center"><img src="https://gitlab.com/sudheer2015/smart-queue-sensing/raw/master/images/IOT.gif" alt="Video demo"/></p>

## Android app Demo

<p align="center"><img src="https://gitlab.com/sudheer2015/smart-queue-sensing/raw/master/images/android_app.gif" alt="Android app demo"/></p>

## Instructions

You can run the application using the below command.

> python Queue_sensing_01.py

It launches the *'Smart queue sensing'* application with two options.

* **Test from Camera:** This function starts looping camera and captures images for every 10000 ms. The face detection inference engine counts faces and updates firebase server.
* **Display analytics:** This function displays count of people in queue for the past 24 hours.  

> Note: Exit the camera loop by pressing **'Esc' key** on the window. This will terminate the loop. Press the 'Test from Camera' button to start loop again.
